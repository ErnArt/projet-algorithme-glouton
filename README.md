# Projet Algorithme Glouton

Projet de M1 sur les Algorithme Glouton en Algorithmie Avancée.

## Compilation

```sh
mkdir build
cd build
cmake ..
make
```

## Utilisation

```sh
./main cheminDuFichier/nomDuFichier.dat sortAUtiliser methodeHotelDeVille
```

Le fichier peut être un .txt ou un .dat

Les différents tris utilisables sont : 
- 0 - Aucun tri
- 1 - Le tri par rapport au bâtiment avec l'air la grande
- 2 - Le tri par rapport au bâtiment le plus encombrant
- 3 - Le tri aléatoire

Les différentes manière de placer l'hôtel de ville
- 0 - En haut à gauche
- 1 - Au milieu
- 2 - En bas à droite

## Script Shell

Nous avons écrit un script shell pour être capable de lancer pour toutes les instances possibles tous les tests avec les différements paramètres en répétant 100 fois chaque tri aléatoire avec une seconde d'écart pour éviter les mêmes valeurs. Toutes ces valeurs sont écrites dans un .csv qui est envoyé par mail à l'adresse mail indiquée.