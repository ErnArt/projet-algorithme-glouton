#include "Building.hpp"

Building::Building(int length, int width, int index, Color::Code color) : 
	index(index),
	length(length), 
	width(width),
	isStored(false),
	coordinates(std::make_pair(0, 0)),
	color(color)
	{}

Building::Building(const Building & copiedBuilding) :
	index(copiedBuilding.index),
	length(copiedBuilding.length), 
	width(copiedBuilding.width), 
	isStored(copiedBuilding.isStored),
	coordinates(copiedBuilding.coordinates),
	color(copiedBuilding.color)
	{ }

int Building::getIndex() {
	return index;
}

Color::Code Building::getColor() {
	return color;
}

std::pair<int, int> Building::getDimension() {
	return std::make_pair(length, width);
}


std::pair<int, int> Building::getCoordinates() {
	return coordinates;
}

int Building::getArea() {
	return this->length * this->width;
}

int Building::getHeigthPlusWidth() {
	return this->length + this->width;
}

void Building::setCoordinates(int row, int column) {
	coordinates = std::make_pair(row, column);
	isStored = true;
}

bool Building::getIsStored(){
	return this->isStored;
}

void Building::printBuilding() {
	Color::Modifier buildingColor(color);
	Color::Modifier defaultColor(Color::colorDefault);
	std::cout<<buildingColor<<"Bâtiment "<<index<<" : "<<length<<", "<<width<<" => ";
	if (isStored)
		std::cout<<"placé";
	else
		std::cout<<"non placé";
	std::cout<<" ("<<coordinates.first<<", "<<coordinates.second<<")"<<defaultColor<<std::endl;
}

bool operator== (const Building & b1, const Building & b2) {
	return (b1.index == b2.index);
}