#ifndef COLOR_HPP_
#define COLOR_HPP_

#include <ostream>
#include <vector>

namespace Color {
	enum Code {
		colorRed			= 31,
		colorGreen			= 32,
		colorYellow			= 33,
		colorBlue			= 34,
		colorMagenta		= 35,
		colorCyan			= 36,
		colorDefault		= 39,
		colorDarkGray		= 90,
		colorLightRed		= 91,
		colorLightGreen		= 92,
		colorLightYellow	= 93,
		colorLightBlue		= 94,
		colorLightMagenta	= 95,
		colorLightCyan		= 96
	};

	class ColorVector {
		public :
			std::vector<Code> colorVector;

			ColorVector() {
				colorVector.push_back(colorRed);
				colorVector.push_back(colorGreen);
				colorVector.push_back(colorYellow);
				colorVector.push_back(colorBlue);
				colorVector.push_back(colorMagenta);
				colorVector.push_back(colorCyan);
				colorVector.push_back(colorDarkGray);
				colorVector.push_back(colorLightRed);
				colorVector.push_back(colorLightGreen);
				colorVector.push_back(colorLightYellow);
				colorVector.push_back(colorLightBlue);
				colorVector.push_back(colorLightMagenta);
				colorVector.push_back(colorLightCyan);
				colorVector.push_back(colorRed);
				colorVector.push_back(colorGreen);
				colorVector.push_back(colorYellow);
				colorVector.push_back(colorBlue);
				colorVector.push_back(colorMagenta);
				colorVector.push_back(colorCyan);
				colorVector.push_back(colorDarkGray);
				colorVector.push_back(colorLightRed);
				colorVector.push_back(colorLightGreen);
				colorVector.push_back(colorLightYellow);
				colorVector.push_back(colorLightBlue);
				colorVector.push_back(colorLightMagenta);
				colorVector.push_back(colorLightCyan);
				colorVector.push_back(colorRed);
				colorVector.push_back(colorGreen);
				colorVector.push_back(colorYellow);
				colorVector.push_back(colorBlue);
				colorVector.push_back(colorMagenta);
				colorVector.push_back(colorCyan);
				colorVector.push_back(colorDarkGray);
				colorVector.push_back(colorLightRed);
				colorVector.push_back(colorLightGreen);
				colorVector.push_back(colorLightYellow);
				colorVector.push_back(colorLightBlue);
				colorVector.push_back(colorLightMagenta);
				colorVector.push_back(colorLightCyan);
				colorVector.push_back(colorRed);
				colorVector.push_back(colorGreen);
				colorVector.push_back(colorYellow);
				colorVector.push_back(colorBlue);
				colorVector.push_back(colorMagenta);
				colorVector.push_back(colorCyan);
				colorVector.push_back(colorDarkGray);
				colorVector.push_back(colorLightRed);
				colorVector.push_back(colorLightGreen);
				colorVector.push_back(colorLightYellow);
				colorVector.push_back(colorLightBlue);
				colorVector.push_back(colorLightMagenta);
				colorVector.push_back(colorLightCyan);
			}

			int size() { return colorVector.size(); }

			std::vector<Code> getColorVector() {
				return colorVector;
			}

			Code pickColor(int index) {
				return colorVector.at(index);
			}

	};

	class Modifier {
		Code code;

		public :
			Modifier(Code pCode) : code(pCode) {}
			friend std::ostream &
			operator<<(std::ostream & os, const Modifier & mod) {
				return os<<"\033["<<mod.code<<"m";
			}
	};
}

#endif