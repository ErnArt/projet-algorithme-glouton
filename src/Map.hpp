#ifndef MAP_HPP_
#define MAP_HPP_

#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include "Building.hpp"

class Map {
	private:
		std::vector<std::vector<int>> groundVector;
		std::pair<int, int> groundSize;
		std::vector<Building> buildingVector;
		int buildingVectorSize;

	public:
		Map(std::string fileName);
		std::vector<Building> getBuildingVector();
		void addBuildingToGround(Building building, int row, int column);
		std::pair<int, int> getDimension();
		std::vector<std::vector<int>> getGroundVector();
		void setGroundVector(int row, int column, int index);
		int evalScore();
		void printMap();

		//Sorting methods (buildingVector)
		void sortBuildingVector(int i);
		void biggestAreaFirstSort();
		void largestPlusLongestFirstSort();
		void randomSort();
};

#endif