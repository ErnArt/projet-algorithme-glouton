#include <iostream>
#include "Building.hpp"
#include "Map.hpp"
#include "Color.hpp"
#include "Glouton.hpp"

using namespace std;

int main(int argc, char* argv[]) {

	if(argc < 4 or argc > 4) {
		cout<<"Trop peu d'arguments, vous devez avoir :"<<endl;
		cout<<"./main cheminDuFichier/nomDuFichier.txt sortAUtiliser methodeHotelDeVille"<<endl<<endl;
		cout<<"Le fichier peut être un .txt ou un .dat"<<endl<<endl;
		cout<<"Les différents tris utilisables sont : "<<endl;
		cout<<"\t0 - Aucun tri"<<endl;
		cout<<"\t1 - Le tri par rapport au bâtiment avec l'air la plus grande"<<endl;
		cout<<"\t2 - Le tri par rapport au bâtiment le plus encombrant"<<endl;
		cout<<"\t3 - Le tri aléatoire"<<endl;
		cout<<endl;
		cout<<"Les différentes manière de placer l'hôtel de ville"<<endl;
		cout<<"\t0 - En haut à gauche"<<endl;
		cout<<"\t1 - Au milieu"<<endl;
		cout<<"\t2 - En bas à droite"<<endl;
		return EXIT_FAILURE;
	}

	// Importing data to the map
	Map map(argv[1]);

	// Sorting with the given argument
	map.sortBuildingVector(stoi(argv[2]));

	//Initiating our greedy algorithm
	Glouton glouton(map);

	//Running to place the previous loaded building data
	glouton.run(stoi(argv[3]));

	//Printing the result of our algorithm
	glouton.getMap().printMap();

	std::cout << "Score: " << glouton.getMap().evalScore() << std::endl;
}