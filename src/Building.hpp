#ifndef BUILDING_HPP_
#define BUILDING_HPP_

#include <iostream>
#include <utility>
#include "Color.hpp"

class Building {
	private:
		int index;
		int length, width;
		std::pair<int, int> coordinates;
		bool isStored;
		Color::Code color;

	public:
		Building(int length, int width, int index, Color::Code color);
		Building(const Building & copiedBuilding);
		int getIndex();
		Color::Code getColor();
		std::pair<int, int> getDimension();
		std::pair<int, int> getCoordinates();
		void setCoordinates(int row, int column);
		bool getIsStored();
		int getArea();
		int getHeigthPlusWidth();
		void printBuilding();
		
		friend bool operator== (const Building & b1, const Building & b2);
};

#endif