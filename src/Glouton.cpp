#include "Glouton.hpp"

Glouton::Glouton(Map map) : 
	map(map)
	{}

int Glouton::addBuildingToMap(Building building){
	//We have to test all coordinate 
	int row = 0;
	int column = 0;

	//While there is a conflit, we need to look for a correct coordinate 
	while(!this->verifConflit(building, row, column)){
		//std::cout << "Row : "<< row << " Column : " << column << std::endl;
		column++;
		if( column > this->map.getDimension().first -1){
			column = 0;
			row++;
		}

		if( row > this->map.getDimension().second -1){
			//We took a look at each coordinate, there is no place to fit the building
			return 1;
		}
	}
	//If there is not conflict, we can place the bulding
	this->map.addBuildingToGround(building, row, column);

	//Success, we placed the building
	return 0;
}

bool Glouton::verifConflit(Building building, int row, int column ){
	for (int i=row; i<row+building.getDimension().first; i++) {
		for (int j=column; j<column+building.getDimension().second; j++){

			//Verify is we aren't out of bounds 
			if(j > this->map.getDimension().first -1 || i > this->map.getDimension().second -1)
				return false;
			
			//If the area isn't empty 
			if(this->map.getGroundVector().at(i).at(j) != 0)
				return false; 
		}
	}

	//If we get here, it means that the area is clear and viable to welcome the building
	//Now, we have to verify that, if we place the building here, the path to to the town hall is still correct
	//If so, we will return true and the building will be place
	//If not, we will return false and the building won't be place, we will look for another area
	if(verifPath(building, row, column))
		return true;
	else
		return false;
}

bool Glouton::verifPath(Building building, int row, int column){

	//We have to work with a temporary copy of the map, as we will temporary change some coordinates to -1
	Map copyMap = this->getMap();

	//We need to temporary add the current building to the location we are looking for
	//If all the buildings will be checked,  we will return true
	//If not, we will return false
	copyMap.addBuildingToGround(building, row, column);

	//Get a copy of all of the other building 
	//We will create a temporary boolean vector to mark them (0 not checked / 1 checked)
	std::vector<Building> buildingTemp = copyMap.getBuildingVector();
	std::vector<bool> buildingTempCheckState;
	for(unsigned int i = 0 ; i < buildingTemp.size() ; i++ ){
		buildingTempCheckState.push_back(false);
	}

	//Get The dimension of the town hall 
	std::pair<int, int> hallDimension = copyMap.getBuildingVector().at(0).getDimension();

	//Get The coordiantes of the town hall
	std::pair<int, int> hallCoordinates = copyMap.getBuildingVector().at(0).getCoordinates();

	//Stack all the area around the town hall
	for(int i = hallCoordinates.first-1 ; i < hallCoordinates.first + hallDimension.first + 1 ; i++){
		for(int j = hallCoordinates.second-1 ; j < hallCoordinates.second + hallDimension.second + 1 ; j++){
			//Verify if we aren't out of bound and if the area is empty
			if( i >= 0 && i <= copyMap.getDimension().first -1 
				&& j >= 0 && j <= copyMap.getDimension().second -1
				&& copyMap.getGroundVector().at(i).at(j) == 0 ){
					//Mark the area with -1 (As we checked it)
					copyMap.setGroundVector(i,j,-1);

					//Stack the area 
					this->stack.push(std::make_pair(i, j));
			}
		}
	}

	//Now, we have to inspect each stacked area
	while(!stack.empty()){
		//Unstack the element on the top
		int row = stack.top().first;
		int column = stack.top().second;
		stack.pop();

		//Look at the temp coordinates in each 4 ways (Upper, bottom, left, right)
		//If the area is empty, stack and mark it

		//Upper
		if( row-1 >= 0 && copyMap.getGroundVector().at(row-1).at(column) == 0){
			//Mark the area with -1 (As we checked it)
			copyMap.setGroundVector(row-1, column, -1);

			//Stack the area 
			this->stack.push(std::make_pair(row-1, column));
		}
		else{
			if( row-1 >= 0 && copyMap.getGroundVector().at(row-1).at(column) != -1){
				//It was a building, we need to mark the building as linked with the hall
				for(int k = 0; k < buildingTemp.size() ; k++)
					if( copyMap.getGroundVector().at(row-1).at(column) == buildingTemp.at(k).getIndex() )
						buildingTempCheckState.at(k) = true;
			}
		}

		//Bottom
		if( row+1 <= copyMap.getDimension().first -1 && copyMap.getGroundVector().at(row+1).at(column) == 0){
			//Mark the area with -1 (As we checked it)
			copyMap.setGroundVector(row+1, column, -1);

			//Stack the area 
			this->stack.push(std::make_pair(row+1, column));
		}
		else{
			if( row+1 <= copyMap.getDimension().first -1 && copyMap.getGroundVector().at(row+1).at(column) != -1){
				//It was a building, we need to mark the building as linked with the hall
				for(int k = 0; k < buildingTemp.size() ; k++)
					if( copyMap.getGroundVector().at(row+1).at(column) == buildingTemp.at(k).getIndex() )
						buildingTempCheckState.at(k) = true;
			}
		}
		
		//Left
		if( column-1 >= 0 && copyMap.getGroundVector().at(row).at(column-1) == 0){
			//Mark the area with -1 (As we checked it)
			copyMap.setGroundVector(row, column-1, -1);

			//Stack the area 
			this->stack.push(std::make_pair(row, column-1));
		}
		else{
			if( column-1 >= 0 && copyMap.getGroundVector().at(row).at(column-1) != -1){
				//It was a building, we need to mark the building as linked with the hall
				for(int k = 0; k < buildingTemp.size() ; k++)
					if( copyMap.getGroundVector().at(row).at(column-1) == buildingTemp.at(k).getIndex() )
						buildingTempCheckState.at(k) = true;
			}
		}

		//Right
		if( column+1 <= copyMap.getDimension().second -1 && copyMap.getGroundVector().at(row).at(column+1) == 0){
			//Mark the area with -1 (As we checked it)
			copyMap.setGroundVector(row, column+1, -1);

			//Stack the area 
			this->stack.push(std::make_pair(row, column+1));
		}
		else{
			if( column+1 <= copyMap.getDimension().second -1 && copyMap.getGroundVector().at(row).at(column+1) != -1){
				//It was a building, we need to mark the building as linked with the hall
				for(int k = 0; k < buildingTemp.size() ; k++)
					if( copyMap.getGroundVector().at(row).at(column+1) == buildingTemp.at(k).getIndex() )
						buildingTempCheckState.at(k) = true;
			}
		}
	}

	//Test to see if the temporary map is correct (with -1)
	// copyMap.printMap();

	//Here, we should change the ground : put all the temporary -1 back to 0
	//copyMap.makeBackToZero();
	
	//Look throught our bool vector to see if every placed building are checked
	for(unsigned int i = 0 ; i < buildingTempCheckState.size() ; i++)
		if(buildingTemp.at(i).getIsStored() && buildingTempCheckState.at(i) == false)
			return false;

	//If we get here, it means that there is a correct path to link the building to the town hall
	return true;
}

Map Glouton::getMap(){
	return this->map;
}

bool Glouton::placeTownhall(Building building, int townhallMethod){
	//First we need to verify if the Townhall isn't bigger than the map
	if(building.getDimension().first > this->getMap().getDimension().first
		|| building.getDimension().second > this->getMap().getDimension().second)
	{
		return false;
	}


	int tempRow = 0;
	int tempCol = 0;
	//We can place the building with the manner we want
	switch (townhallMethod)
	{
	
	//(0) : Left upper corner 
	case 0:
		this->map.addBuildingToGround(building, 0, 0);
		break;
	
	//(1) : Middle 
	case 1:
		while(this->getMap().getDimension().first - building.getDimension().first - tempRow> this->getMap().getDimension().first / 2
		|| this->getMap().getDimension().second - building.getDimension().second - tempCol > this->getMap().getDimension().second /2
		){
			if(this->getMap().getDimension().first - building.getDimension().first - tempRow> this->getMap().getDimension().first /2)
				tempRow++;

			if(this->getMap().getDimension().second - building.getDimension().second - tempCol >this->getMap().getDimension().second /2)
				tempCol++;
		}
		this->map.addBuildingToGround(building, this->getMap().getDimension().first - building.getDimension().first - tempRow - building.getDimension().first /2, this->getMap().getDimension().second - building.getDimension().second - tempCol - building.getDimension().second /2);
		break;

	//(2) : Right bottom corner 
	case 2: 
		this->map.addBuildingToGround(building, this->getMap().getDimension().first - building.getDimension().first, this->getMap().getDimension().second - building.getDimension().second);
		break;

	default:
		break;
	}

	return true;
}

void Glouton::run(int townhallMethod){
	Color::Modifier colorDefault(Color::colorDefault);
	for(int i = 0; i < map.getBuildingVector().size(); i++){
		Color::Modifier currentColor(map.getBuildingVector().at(i).getColor());

		//The townhall is a specific main building, we can choose how to place it
		if(map.getBuildingVector().at(i).getIndex() == 1){
			if(this->placeTownhall(this->map.getBuildingVector().at(i), townhallMethod) == false){
				std::cout<<"Le batiment principal est beaucoup trop grand ! " << std::endl;
				std::cout<<"Veillez verifier vos dimensions" << std::endl;
				break;
			}
		}		
		else{
			//Placing other buildings
			std::cout << currentColor << "Le batiment d'index" << map.getBuildingVector().at(i).getIndex();
			if(this->addBuildingToMap(this->map.getBuildingVector().at(i)) == 1)
				std::cout<<" n'a pas pu être placé";
			else
				std::cout<<" a été placé avec succès";
			std::cout<<colorDefault<< std::endl;
		}
	}
}