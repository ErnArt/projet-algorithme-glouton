#include "Map.hpp"

Map::Map(std::string fileName) {
	std::fstream file;
	file.open(fileName, std::ios::in);
	if (file.is_open()) {
		std::string tempString1, tempString2;
		file >> tempString1 >> tempString2;
		groundSize.first = stoi(tempString1);
		groundSize.second = stoi(tempString2);

		file >> tempString1;
		int buildingVectorSize = std::stoi(tempString1);
		this -> buildingVectorSize = buildingVectorSize;

		Color::ColorVector colorVector;

		for (int i=0; i<buildingVectorSize; i++) {
			file >> tempString1 >> tempString2;
			if (buildingVector.size() != buildingVectorSize)
				buildingVector.push_back(Building(stoi(tempString1), stoi(tempString2), i+1, colorVector.pickColor(i)));
		}

		groundVector = std::vector<std::vector<int>> (groundSize.first, std::vector<int>(groundSize.second, 0));
	}
	else {
		std::cout<<"File can't open or doesn't exist"<<std::endl;
		return;
	}
}

std::vector<Building> Map::getBuildingVector() {
	return buildingVector;
}

void Map::addBuildingToGround(Building building, int row, int column) {
	for (int i=row; i<row+building.getDimension().first; i++) {
		for (int j=column; j<column+building.getDimension().second; j++)
			groundVector.at(i).at(j) = building.getIndex();
	}

	for (auto & i : buildingVector)
		if (i == building)
			i.setCoordinates(row, column);
}

std::pair<int, int> Map::getDimension() {
	return this->groundSize;
}

std::vector<std::vector<int>> Map::getGroundVector(){
	return this->groundVector;
}

void Map::setGroundVector(int row, int column, int index){
	this->groundVector.at(row).at(column) = index;
}

int Map::evalScore(){

	int score = 0;

	for (int i=0; i<groundVector.size(); i++) {
		for (int j=0; j<groundVector.at(i).size(); j++)
			if(groundVector.at(i).at(j) != 0)
				score++;
	}

	return score;
}

void Map::printMap() {
	std::cout<<"Terrain : "<<groundSize.first<<", "<<groundSize.second<<std::endl;
	
	std::cout<<"Nombre de bâtiments : "<<buildingVectorSize<<std::endl;
	for (auto i : buildingVector)
		i.printBuilding();

	Color::Modifier colorDefault(Color::colorDefault);

	for (int i=0; i<groundVector.size(); i++) {
		for (int j=0; j<groundVector.at(i).size(); j++) {
			if (groundVector.at(i).at(j) > 0) {
				Color::Modifier currentColor(buildingVector.at(groundVector.at(i).at(j)-1).getColor());
				std::cout<<currentColor;
			}
			if (groundVector.at(i).at(j) < 10)
				std::cout<<" ";
			std::cout<<groundVector.at(i).at(j)<<colorDefault<<" ";
		}
		std::cout<<std::endl;
	}
}

void Map::sortBuildingVector(int i) {
	switch (i) {
		case 1:
			biggestAreaFirstSort();
			break;
		case 2:
			largestPlusLongestFirstSort();
			break;
		case 3:
			randomSort();
			break;
	}
}

void Map::biggestAreaFirstSort(){
	int tempArea = 0;
	int tempIndex = 0;

	//We start at 1 because the first building is always the townHall
	int compt = 1 ;

	while(compt != this->buildingVector.size()){
		//Look for the current biggest
		for(unsigned int i = compt; i < this->buildingVector.size(); i++){
			if(this->buildingVector.at(i).getArea() > tempArea){
				tempArea = this->buildingVector.at(i).getArea();
				tempIndex = i; 
			}
		}

		//Swapping
		//iter_swap(this->buildingVector.begin() + compt, this->buildingVector.begin() + tempIndex);
		std::swap(this->buildingVector.at(compt), this->buildingVector.at(tempIndex));

		//Don't forget to reset variables and increase compt
		tempArea = 0;
		compt++;
	}
}

void Map::largestPlusLongestFirstSort(){
	int tempHeightPlusWidth = 0;
	int tempIndex = 0;

	//We start at 1 because the first building is always the townHall
	int compt = 1 ;

	while(compt != this->buildingVector.size()){
		//Look for the current longuest + largest
		for(unsigned int i = compt; i < this->buildingVector.size(); i++){
			if(this->buildingVector.at(i).getHeigthPlusWidth() > tempHeightPlusWidth){
				tempHeightPlusWidth = this->buildingVector.at(i).getHeigthPlusWidth();
				tempIndex = i; 
			}
		}

		//Swapping
		iter_swap(this->buildingVector.begin() + compt, this->buildingVector.begin() + tempIndex);
		
		//Don't forget to reset variables and increase compt
		tempHeightPlusWidth = 0;
		compt++;
	}
}

void Map::randomSort(){
	srand (time(NULL));

	int randomIndex1, randomIndex2;
	int tempIndex = 0;

	//We start at 1 because the first building is always the townHall 
	int compt = 1 ;

	while(compt != this->buildingVector.size()){
		//Generate two correct random numbers between 1 and max size
		randomIndex1 = rand() % (this->buildingVector.size() -1) + 1;
		randomIndex2 = rand() % (this->buildingVector.size() -1) + 1;

		//Random Swapping
		iter_swap(this->buildingVector.begin() + randomIndex1, this->buildingVector.begin() + randomIndex2);
		
		//Don't forget to increase compt
		compt++;
	}
}