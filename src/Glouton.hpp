#ifndef GLOUTON_HPP_
#define GLOUTON_HPP_

#include <iostream>
#include <utility>
#include <vector>
#include <stack>
#include "Building.hpp"
#include "Map.hpp"
#include "Color.hpp"

class Glouton {
	private:
		Map map;
		std::stack<std::pair<int, int>> stack;

	public:
		Glouton(Map map);
		int addBuildingToMap(Building building);
		bool verifConflit(Building building, int row, int column);
		bool verifPath(Building building, int row, int column);
		Map getMap();
		bool placeTownhall(Building building, int townhallMethod);
		void run(int townhallMethod);
};

#endif