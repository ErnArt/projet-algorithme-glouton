#! /bin/bash

echo "instance, tri, placement, resultat" > result.csv
for file in instances/*
	do
		for sort in {0..3}
			do
				for placement in {0..2}
					do
						if [ $sort == 3 ]
							then
								for number in {0..100}
									do
										sleep 1
										result=$(build/main $file $sort $placement)
										line=$file", "$sort", "$placement", "$result
										echo $line >> result.csv
									done
						else
							result=$(build/main $file $sort $placement)
							line=$file", "$sort", "$placement", "$result
							echo $line >> result.csv
						fi
					done
			done
	done

mail -s "Résultats de l'algorithme" mail.com <<< "Voilà les résultats de l'algorithme." -A result.csv